package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

// Response ...
type Response struct {
	Success bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Error   interface{} `json:"error"`
}

func version(w http.ResponseWriter, r *http.Request) {
	var response Response
	response.Error = false
	response.Message = "success"
	response.Success = true
	response.Data = "v1"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func login(w http.ResponseWriter, r *http.Request) {

}

func register(w http.ResponseWriter, r *http.Request) {

}

func main() {

	router := mux.NewRouter()
	router.HandleFunc("/version", version).Methods("GET")
	router.HandleFunc("/login", login).Methods("POST")
	router.HandleFunc("/register", register).Methods("POST")

	http.Handle("/", router)
	fmt.Println("SERVICE RUNNING ON 127.0.01:1234")
	log.Fatal(http.ListenAndServe(":1234", router))

}
